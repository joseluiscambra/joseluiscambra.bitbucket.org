function test() {
    var text = '';
    var support_audio = Modernizr.audio;
    var support_canvas = Modernizr.canvas;
    var support_geo = Modernizr.geolocation;
    
    if (support_audio) {
        text += '<br>Soporta audio';
    }
    else {
        text += '<br>No soporta audio';
    }
    
    if (support_canvas) {
        text += '<br>Soporta canvas';
    }
    else {
        text += '<br>No soporta canvas';
    }
    
    if (support_geo) {
        text += '<br>Soporta geolocalización';
    }
    else {
        text += '<br>No soporta geolocalización';
    }
    
    document.getElementById('message').innerHTML = text;
}
