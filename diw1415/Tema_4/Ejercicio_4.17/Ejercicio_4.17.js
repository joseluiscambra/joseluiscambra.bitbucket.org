function test() {
    var text = '';
    var support_video = Modernizr.video;
    
    if (support_video) {
        text += '<br/>Soporta video';
    } else {
        text += '<br/>No soporta video';
    }
    
    document.getElementById('message').innerHTML = text;
}