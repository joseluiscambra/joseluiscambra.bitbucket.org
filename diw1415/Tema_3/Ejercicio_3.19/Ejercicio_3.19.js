function validate_cp() {
    var regexp = /^[0-9]{5}$/;
    var cod_pos = document.getElementById('codigopostal');

    if (!regexp.test(cod_pos.value)) {
        cod_pos.setAttribute('class', 'mal');
    }
    else {
        cod_pos.removeAttribute('class');
    }

    return regexp.test(cod_pos.value);
}

function validate_email() {
    var regexp = /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})$/;
    var email = document.getElementById('email');

    if (!regexp.test(email.value)) {
        email.setAttribute('class', 'mal');
    }
    else {
        email.removeAttribute('class');
    }

    return regexp.test(email.value);
}

function validate_dni() {
    var result = false;
    var regexp = /^[XYZ]\d{7,8}|^\d{8}[A-HJ-NP-TV-Z]$/i;
    var dni = document.getElementById('dni');

    if (regexp.test(dni.value)) {
        var letras = "TRWAGMYFPDXBNJZSQVHLCKET";

        var modulo = dni.value.substring(0, 8);

        modulo = modulo % 23;

        var letra = letras[modulo];

        var letraM = dni.value.toUpperCase();

        if (letra !== letraM[8]) {
            result = false;
        } else {
            result = true;
        }
    }

    if (!result) {
        dni.setAttribute('class', 'mal');
    }
    else {
        dni.removeAttribute('class');
        dni.setAttribute('class', 'titulo');
    }

    return result;
}

function validate_name() {
    var regexp = /^[a-zA-ZñÑ\\s]+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)/;
    var nombre = document.getElementById('nombre');

    if (!regexp.test(nombre.value)) {
        nombre.setAttribute('class', 'mal');
    }
    else {
        nombre.removeAttribute('class');
    }

    return regexp.test(nombre.value);
}

function validate_surname1() {
    var regexp = /^[a-zA-ZñÑ\\s]+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)/;
    var apellido1 = document.getElementById('apellido1');

    if (!regexp.test(apellido1.value)) {
        apellido1.setAttribute('class', 'mal');
    }
    else {
        apellido1.removeAttribute('class');
    }

    return regexp.test(apellido1.value);
}

function validate_surname2() {
    var regexp = /^[a-zA-ZñÑ\\s]+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)/;
    var apellido2 = document.getElementById('apellido2');

    if (!regexp.test(apellido2.value)) {
        apellido2.setAttribute('class', 'mal');
    }
    else {
        apellido2.removeAttribute('class');
    }

    return regexp.test(apellido2.value);
}

function validate_address() {
    var regexp = /^[a-zA-Z0-9\s,'-]*$/;
    var direccion = document.getElementById('direccion');

    if (!regexp.test(direccion.value)) {
        direccion.setAttribute('class', 'mal');
    }
    else {
        direccion.removeAttribute('class');
    }

    return regexp.test(direccion.value);
}

function validate_city() {
    var regexp = /^[a-zA-ZñÑ\\s]+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)+([ ]?)+([a-zA-Z]*)/;
    var municipio = document.getElementById('municipio');

    if (!regexp.test(municipio.value)) {
        municipio.setAttribute('class', 'mal');
    }
    else {
        municipio.removeAttribute('class');
    }

    return regexp.test(municipio.value);
}

function validate_province() {
    var provincia = document.getElementById('provincia');

    if (provincia.value === '') {
        provincia.setAttribute('class', 'mal');
    }
    else {
        provincia.removeAttribute('class');
    }

    return provincia.value;
}

function validate_country() {
    var pais = document.getElementById('pais');

    if (pais.value === '') {
        pais.setAttribute('class', 'mal');
    }
    else {
        pais.removeAttribute('class');
    }

    return pais.value;
}

function validate_phone_number() {
    var regexp = /^9\d{8}$/;
    var telefonofijo = document.getElementById('telefonofijo');

    if (!regexp.test(telefonofijo.value)) {
        telefonofijo.setAttribute('class', 'mal');
    }
    else {
        telefonofijo.removeAttribute('class');
    }

    return regexp.test(telefonofijo.value);
}

function validate_mobile_phone_number() {
    var regexp = /^(6|7)\d{8}$/;
    var telefonomovil = document.getElementById('telefonomovil');

    if (!regexp.test(telefonomovil.value)) {
        telefonomovil.setAttribute('class', 'mal');
    }
    else {
        telefonomovil.removeAttribute('class');
    }

    return regexp.test(telefonomovil.value);
}

function validate_sex() {
    var result = false;
    var sexo = document.getElementById('sexos');
    var hombre = document.getElementById('hombre');
    var mujer = document.getElementById('mujer');

    if (hombre.checked || mujer.checked) {
        result = true;
        sexo.removeAttribute('class');
        sexo.setAttribute('class', 'titulo');
    }
    else {
        result = false;
        sexo.setAttribute('class', 'mal');
    }

    return result;
}

function validate_form() {
    var result = '';
    var nombre = validate_name();
    var apellido1 = validate_surname1();
    var apellido2 = validate_surname2();
    var direccion = validate_address();
    var cod_postal = validate_cp();
    var municipio = validate_city();
    var provincia = validate_province();
    var pais = validate_country();
    var email = validate_email();
    var telefono_fijo = validate_phone_number();
    var telefono_movil = validate_mobile_phone_number();
    var dni = validate_dni();
    var sexo = validate_sex();

    if (!nombre) {
        result += "- Nombre incorrecto\n";
    }

    if (!apellido1) {
        result += "- Primer apellido incorrecto\n";
    }

    if (!apellido2) {
        result += "- Segundo apellido incorrecto\n";
    }

    if (!direccion) {
        result += "- Dirección incorrecta\n";
    }

    if (!cod_postal) {
        result += "- Código postal incorrecto\n";
    }

    if (!municipio) {
        result += "- Municipio incorrecto\n";
    }

    if (provincia === '') {
        result += "- Provincia incorrecta\n";
    }

    if (pais === '') {
        result += "- País incorrecto\n";
    }

    if (!email) {
        result += "- Email incorrecto\n";
    }

    if (!telefono_fijo) {
        result += "- Teléfono fijo incorrecto\n";
    }

    if (!telefono_movil) {
        result += "- Teléfono móvil incorrecto\n";
    }

    if (!dni) {
        result += "- DNI incorrecto\n";
    }

    if (!sexo) {
        result += "- Sexo incorrecto\n";
    }
}