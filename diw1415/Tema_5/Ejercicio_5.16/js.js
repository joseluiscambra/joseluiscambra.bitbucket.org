$(function() {
    $.vegas('slideshow', {
        delay: 5000,
        backgrounds:[
            { src:'img/001.jpg' },
            { src:'img/002.jpg' },
            { src:'img/003.jpg' },
            { src:'img/004.jpg' },
            { src:'img/005.jpg' }
        ]
    });
    
    $('body').bind('vegascomplete', 
        function(e, bg) {
            var img_src = $(bg).attr('src');
            var img = $('div[id="' + img_src + '"]');
            if (img !== undefined && img !== null) {
                $('.info').removeClass('active');
                var child = img.children();
                var div = child.last();
                div.removeClass('info');
                div.addClass('active');
                div.addClass('info');
                
                var slide = img.children().children().children();
                
                exit_slide();
                enter_slide($(slide).attr('alt'));
            }
        }
    );
    
    $('#toggle').on('click', function() {
        my_toggle();
    });
    
    $('.minitoggle').on('click', function() {
        var step = $(this).attr('alt') - 1;
        $.vegas('jump', step);
    });
    
    my_toggle();
});

function my_toggle() {
    if ($('#content').css('opacity') == 0) {
        $('#content').css('top', '100%');
        $('#content').animate({ 
            top: '0%',
            opacity:'1'
        },
        3000);
    }
    else {
        $('#content').animate({ 
            top:'100%',
            opacity:'0'
        },
        3000,
        "linear",
        function() {
            $('#content').css('top', '0%');
        });
    }
}

$(document).ready(function() {
    my_toggle();
});

function exit_slide() {
    var img = $('.step').filter(function () { 
        return this.style.opacity == 1;
    });
    if (img.length > 0) {
        var newstep = img[0].id.substring(4);    
        $("#step" + newstep).animate({ 
            left:'-100%',
            opacity:'0'
        },
        1700,
        "linear");
    }
}

function enter_slide(step) {
    if ($("#step" + step).position().left !== 800) {
        $("#step" + step).css('left', 800 + 'px');
    }
        
    var newleft = 0;
    $.each($('.step'), function() {
        var value = this.id.substring(4);
        if (value < step) {
            newleft += $(this).width();
        }
    });
    
    $("#step" + step).animate({ 
        //top:'0px',
        left:'300px',
        opacity:'1'
    },
    1000,
    "linear");
}